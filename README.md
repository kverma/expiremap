# **Expire Map** #

## Concurrent, Threadsafe Map like data structure which removes entries after their timeout. ##

## ***API*** ##

### **put** ###
* Put <Key, Value> entries to the map with a timeout (milliseconds).
* Entries with positive timeout are scheduled for removal.
* Multiple puts with the same key and/or value with different timeouts is supported. Last update is taken for scheduling the expiration.
* Null Keys not permitted. Null value allowed.
* Timeout cannot be negative. If zero (0), then entry never expires, in fact no associated removal is scheduled.
* After the timeout for a given entry in the map expires, that entry is automatically removed from the map.

### **remove** ###
* The entry can be removed explicitly before the expiration using remove(key).
* Null key for removal not permitted.
* No race conditions between expiration and explicit removal/gets.

### **get** ###
* Get an entry by the key, returns the value associated with the key if that key exists in the map and only if the entry has not expired. Else null is returned. 
* Null can also bed returned if the last put for the same key had a value null.

## ***Big O Complexity*** ##
* put - O(log n) based on underlying [ConcurrentSkipListMap](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ConcurrentSkipListMap.html) usage.
* remove - O(log n) based on underlying [ConcurrentSkipListMap](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ConcurrentSkipListMap.html) usage.
* get - O(1) based on underlying [ConcurrentHashMap](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ConcurrentHashMap.html) usage.


## ***Implementation Details*** ##

Two Maps used for storage:

* BackingStore which is a ConcurrentHashMap: to store the actual Key/Value entry.
* ExpirationQueue which is a ConcurrentSkipListMap: to store the [ExpirableEntry](https://bitbucket.org/kverma/expiremap/src/master/src/main/java/org/bitbucket/kverma/expiremap/ExpirableEntry.java) in the skiplist based Map, and is sorted by the expiration time. The Expirable Entry is just a pojo/wrapper around the Key/Value Pair, plus utility methods. The expiration time which is used as the Key is the System Time in Nano Seconds resolution, thereby giving a reasonable guarantee on uniqueness.

For each Map, one companion [ReaperThread](https://bitbucket.org/kverma/expiremap/src/master/src/main/java/org/bitbucket/kverma/expiremap/impl/Reaper.java) used for finding and removing expired entries.

The Reaper Thread is in a wakeup->find-expired->flush-expired->sleep loop.

### For every put operation: ###
* Storing the new entry in the ConcurrentHashMap has Key->Value.
* Determining the actual expiration time when this entry should be removed.
* Scheduling the expiration when needed by putting the entry in the expirationQ using the expiration time as the Key and ExpirableEntry as the Value.
* Signalling the Reaper to check and adjust it- wake up time if needed.
* In case put is a replace, then previous entry is just removed from the expiration queue.

### For every get operation: ###
- Retrieve entry from underlying storage
- check if expiration time has passed, if so null is returned, else return the actual value.

### For every remove operation: ###
- Remove entry from underlying storage, and cancel its scheduled expiration if it was scheduled.


## ***Concurrency Design*** ##
* Uses concurrent maps provided by JDK itself for thread-safe access, atomic operations
* The Reaper thread on waking up, finds all entries which have expired using the ConcurrentSkipListMaps.[headMap](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ConcurrentSkipListMap.html#headMap-K-) which gives a view of all entries which are lesser and current time and therefore have expired.
* The Reaper's wake up schedule is determined by the ExpirationQ's [firstKey](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ConcurrentSkipListMap.html#firstKey--).
* All notifications between the callee thread and reaper threads and wake up interruptions are done using JDK's ReentrantLock lock/unlock and associated Condition.await/signal methods.
* Minimal usage of above synchronization directives.

## ***Tests*** ##
* Extensive jUnit test cases to check expiration's, multiple puts/removals, concurrent access.
* Concurrent Access and Load Tests are using google guava Futures and slf4j logging (Test dependency only).
* use mvn verify site to view the test reports.
* Code coverage ~ 90%

## ***Build instruction*** ##
* Clone the git repo, use maven
* mvn clean verify install site and view the mvn site reports.

## ***Documentation*** ##
* Javadocs, code-coverage, findBugs reports are generatable. use maven site command to generate and view.
* if graphviz is installed on the system, the UML class diagrams are embedded in the javadocs.
