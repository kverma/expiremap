/*
 * Copyright (c) 2017. Krish Verma
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.bitbucket.kverma.expiremap;

import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;


/**
 * Map Entry which encapuslates the expiration time stamp
 *
 * @param <K> Key
 * @param <V> Value
 */
public class ExpirableEntry<K, V> {

    private final K key;
    private final V value;
    private final long expirationTimeNs;
    private ConcurrentMap<K, ExpirableEntry<K, V>> map;
    private volatile boolean isExpired;

    /**
     * Expirable Entry/
     *
     * @param key for the map
     * @param value for the map
     * @param map backing store where this entry is actually stored.
     * @param timeoutMs expiration timeout.
     */
    public ExpirableEntry(K key, V value, ConcurrentMap<K, ExpirableEntry<K, V>> map, long timeoutMs) {
        this.key = key;
        this.value = value;
        this.map = map;
        long whenToExpire = System.nanoTime() + TimeUnit.NANOSECONDS.convert(timeoutMs, TimeUnit.MILLISECONDS);
        this.expirationTimeNs = (timeoutMs == 0) ? 0 : ((whenToExpire < 0) ? 0 : whenToExpire); // to handle overflow.
        this.isExpired = false;
    }

    /**
     * Value associated with this key.
     *
     * @return value/
     */
    public V getValue() {
        return value;
    }


    /**
     * When is the expiration scheduled
     *
     * @return System time in NanoSeconds when this entry is expected to be removed.
     */
    public long getExpirationTimeNs() {
        return expirationTimeNs;
    }

    /**
     * Can the entry every expire?
     *
     * @return if the entry has a expiration date.
     */
    public boolean isForeverEntry() {
        return expirationTimeNs == 0;
    }

    /**
     * Helper method to check if the entry has already expired to handle any race conditions between
     * the expiration flow and map.get access flow.
     *
     * @return true if the entry has expired.
     */
    public boolean hasExpired() {
        // if bool flag is not set, re-confirm using the diff
        return isExpired || ((System.nanoTime() - expirationTimeNs) >= 0);
    }

    /**
     * Sets the entry as expired. Optionally removes if from the backing store.
     *
     * @param removeFromMap whether to remove the entry from the backing store or not
     */
    public void setExpired(boolean removeFromMap) {
        this.isExpired = true;
        if (removeFromMap) {
            map.remove(key, this);
        }
    }


    @Override
    public String toString() {
        return "ExpirableEntry{" +
               "key=" + key +
               ", value=" + value +
               ", expirationTimeNs=" + expirationTimeNs +
               '}';
    }
}
