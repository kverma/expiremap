/*
 * Copyright (c) 2017. Krish Verma
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.bitbucket.kverma.expiremap;

/**
 * Interface for the entity which can do the cleanup of expired entries.
 *
 * @param <K> Key Type
 * @param <V> Value Type
 */
public interface CleanupScheduler<K, V> {

    /**
     * Added as a way to not start the thread in the constructor
     */
    void start();

    /**
     * Schedule removal of the Map Entry.
     *
     * @param expirableEntry entry to remove on expiry.
     */
    void scheduleExpiration(ExpirableEntry<K, V> expirableEntry);

    /**
     * Cancel the scheduled removal of the map entry - useful in case the same key is used to insert multiple values.
     *
     * @param expirableEntry entry whose expiration is to be cancelled.
     */
    void cancelScheduledExpiration(ExpirableEntry<K, V> expirableEntry);

    /**
     * Shuts down the internal entity which does the cleanup of expired entries.
     *
     * @throws InterruptedException in case shutdown is interrupted.
     */
    void shutDown() throws InterruptedException;

    /**
     * This method is used to get the # of entries pending expiration at the time of the query.
     *
     * @return Returns the number of entries pending expiration.
     */
    int size();
}
