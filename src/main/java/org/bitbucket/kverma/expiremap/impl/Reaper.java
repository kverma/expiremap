/*
 * Copyright (c) 2017. Krish Verma
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.bitbucket.kverma.expiremap.impl;

import java.util.NoSuchElementException;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.bitbucket.kverma.expiremap.CleanupScheduler;
import org.bitbucket.kverma.expiremap.ExpirableEntry;

/**
 * Class which performs the find-n-destroy expired entries in the Map
 *
 * @param <K> Key Type
 * @param <V> Value Type
 */
public class Reaper<K, V> implements CleanupScheduler<K, V> {

    private static int TID = 1; // ThreadId
    private final Thread reaperThread;
    private final Lock interruptLock;
    private final Condition wakeUpCondition;
    private final ConcurrentNavigableMap<Long, ExpirableEntry<K, V>> expirationQue;
    private volatile boolean shutdown = false;
    private volatile long currentWakeUpTimeNs = Long.MAX_VALUE;

    public Reaper() {
        interruptLock = new ReentrantLock();
        wakeUpCondition = interruptLock.newCondition();
        expirationQue = new ConcurrentSkipListMap<>();
        reaperThread = new Thread(new TerminationTask(), "ExpMap-Reaper-" + (TID++));
        reaperThread.setDaemon(true);
    }

    /**
     * Added as a way to not start the thread in the constructor
     *
     * @throws IllegalThreadStateException in case it is called after shutdown
     */
    @Override
    public void start() {
        if (!shutdown) {
            reaperThread.start();
        } else {
            throw new IllegalThreadStateException();
        }
    }

    /**
     * Schedule removal of the Map Entry.
     *
     * @param expirableEntry entry to remove on expiry.
     * @throws IllegalThreadStateException in case it is called after shutdown
     */
    @Override
    public void scheduleExpiration(ExpirableEntry<K, V> expirableEntry) {
        if (shutdown) {
            throw new IllegalThreadStateException();
        }

        expirationQue.put(expirableEntry.getExpirationTimeNs(), expirableEntry);
        if (getNextWakeUpTimeNs() != currentWakeUpTimeNs) {
            interruptLock.lock();
            try {
                wakeUpCondition.signal();
            } finally {
                interruptLock.unlock();
            }
        }  // else no need to interrupt the snooze as this entry will be automatically picked up
    }

    /**
     * Cancel the scheduled removal of the map entry - useful in case the same key is used to insert multiple values.
     *
     * @param expirableEntry entry whose expiration is to be cancelled.
     * @throws IllegalThreadStateException in case it is called after shutdown
     */
    @Override
    public void cancelScheduledExpiration(ExpirableEntry<K, V> expirableEntry) {
        if (shutdown) {
            throw new IllegalThreadStateException();
        }

        if (expirableEntry != null && !expirableEntry.isForeverEntry()) {
            expirableEntry.setExpired(false);
            expirationQue.remove(expirableEntry.getExpirationTimeNs(), expirableEntry);
        }
    }

    /**
     * This method is used to get the # of entries pending expiration at the time of the query.
     *
     * @return Returns the number of entries pending expiration.
     */
    @Override
    public int size() {
        return expirationQue.size();
    }


    /**
     * Shutsdown the internal entity which does the cleanup of expired entries.
     *
     * @throws InterruptedException in case shutdown is interrupted.
     */
    @Override
    public void shutDown() throws InterruptedException {
        shutdown = true;
        interruptLock.lock();
        try {
            // Signal the reaper to wake up
            wakeUpCondition.signal();
        } finally {
            interruptLock.unlock();
        }
        // Wait for the Reaper to die.
        reaperThread.join();
    }

    /**
     * Gets the next Time to wakeup to do cleanup.
     *
     * @return Long.MAX_VALUE if nothing to expire or the 1st time when the expiration needs to be done.
     */
    private long getNextWakeUpTimeNs() {
        try {
            return expirationQue.firstKey();
        } catch (NoSuchElementException ex) {
            return Long.MAX_VALUE; // wait forever.
        }
    }

    /**
     * Flushes the entries to be expired or flushes all in case of shutdown.
     */
    private void flushExpiredEntries() {
        if (shutdown) {
            // Cleanup the queue
            expirationQue.clear();
        } else {
            ConcurrentNavigableMap<Long, ExpirableEntry<K, V>> expirationCandidates =
                    expirationQue.headMap(System.nanoTime());
            expirationCandidates.values().forEach(candidate -> candidate.setExpired(true));
            expirationCandidates.clear();
        }
    }


    /**
     * Runnable for the Timer Task to clean-up the expired entries.
     */
    final class TerminationTask implements Runnable {

        public void run() {
            while (!shutdown) {
                currentWakeUpTimeNs = getNextWakeUpTimeNs();
                long snoozeInterval = getSnoozeTimeNs(currentWakeUpTimeNs);
                while (snoozeInterval >= 0) {
                    // Snooze interrupted either due to new entry added or shutDown called
                    if (!shutdown && snooze(snoozeInterval)) {
                        currentWakeUpTimeNs = getNextWakeUpTimeNs();
                        snoozeInterval = getSnoozeTimeNs(currentWakeUpTimeNs);
                    } else {
                        // time to wake up and do cleanup.
                        break;
                    }
                }
                flushExpiredEntries();
            }
        }

        /**
         * Determines how long to snooze for in NanoSecs.
         *
         * @param whenToWakeUpNs when was the 1st event scheduled.
         * @return Long.MAX_VALUE in case have to wait forever. If negative, no waiting. Else the snooze Interval
         */
        private long getSnoozeTimeNs(long whenToWakeUpNs) {
            long retVal = Long.MAX_VALUE;
            if (whenToWakeUpNs != Long.MAX_VALUE) {
                retVal = whenToWakeUpNs - System.nanoTime();
                if (retVal == 0) {
                    retVal = -1; // Special Case - fire flushing immediately.
                }
            }
            return retVal;
        }

        /**
         * Snoozes for the given time interval.
         *
         * @param snoozeInterval - how long to snooze for.
         * @return true in case the snoozing was interrupted else false if the snooze was deep deep peaceful restful
         * slumber and it woke up fully refreshed.
         */
        private boolean snooze(long snoozeInterval) {
            boolean snoozeInterrupted;
            interruptLock.lock();
            try {
                snoozeInterrupted = wakeUpCondition.await(snoozeInterval, TimeUnit.NANOSECONDS);
            } catch (InterruptedException e) {
                snoozeInterrupted = true;
            } finally {
                interruptLock.unlock();
            }
            return snoozeInterrupted;
        }

    }
}
