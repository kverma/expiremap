/*
 * Copyright (c) 2017. Krish Verma
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.bitbucket.kverma.expiremap.impl;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.bitbucket.kverma.expiremap.CleanupScheduler;
import org.bitbucket.kverma.expiremap.ExpirableEntry;
import org.bitbucket.kverma.expiremap.ExpireMap;

/**
 * Map like structure which expires entries based on their expiration dates.
 */
public class ExpireMapImpl<K, V> implements ExpireMap<K, V> {

    private final ConcurrentMap<K, ExpirableEntry<K, V>> backingStore; //actual backing store.

    private final CleanupScheduler<K, V> grimReaper;

    ExpireMapImpl(ExpireMapBuilder<K, V> builder) {
        this.backingStore = builder.kvBackingStore;
        this.grimReaper = builder.cleanupScheduler;
        this.grimReaper.start();
    }

    /**
     * If there is no entry with the key in the map, add the key/value pair as a new entry.
     * If there is an existing entry with the key, the current entry will be replaced with the new key/value pair.
     * If the newly added entry is not removed after timeoutMs since it's added to the map, remove it.
     * If timeoutMs is 0, then entry never expires
     *
     * @param key Key for the map entry. Not Null. Throws NPE in case it is null
     * @param value Value for the map. Nullable
     * @param timeoutMs Expiration age of the entry, must be greater than or equal to 0. 0 means never expire.
     * @throws IllegalArgumentException in case of negative timeout.
     * @throws NullPointerException     in case of null key.
     *                                  <p>
     *                                  <p>
     *                                  Big O complexity:  O(log n) - backed by ConcurrentSkipListHashMap.
     */
    @Override
    public void put(K key, V value, long timeoutMs) {
        if (key == null) {
            throw new NullPointerException("Null Key supplied");
        }
        if (timeoutMs < 0) {
            throw new IllegalArgumentException("Negative Timeout supplied for [Key: " + key + ", Val = " + value + "]");
        }

        ExpirableEntry<K, V> newEntry = new ExpirableEntry<>(key, value, backingStore, timeoutMs);
        ExpirableEntry<K, V> prevEntry = backingStore.put(key, newEntry);
        if (!newEntry.isForeverEntry()) {
            grimReaper.scheduleExpiration(newEntry);
        }
        grimReaper.cancelScheduledExpiration(prevEntry);
    }

    /**
     * Get the value associated with the key if present; otherwise, return null.
     *
     * @param key retrieval key
     * @return null in case key is not present, or else has expired otherwise value associated with key.
     * <p>
     * Big O complexity: O(1) - backed by ConcurrentHashMap.
     */
    @Override
    public V get(K key) {
        if (key == null) {
            throw new NullPointerException("Null Key supplied");
        }
        V retVal = null;
        ExpirableEntry<K, V> expirableEntry = backingStore.get(key);
        if (expirableEntry != null && (expirableEntry.isForeverEntry() || !expirableEntry.hasExpired())) {
            retVal = expirableEntry.getValue();
        }
        return retVal;
    }

    /**
     * Remove the entry associated with key, if any.
     *
     * @param key - removes the associated entry if present in the map.
     * <p>
     * Big O complexity: O(log n) - backed by ConcurrentSkipListHashMap.
     */
    @Override
    public void remove(K key) {
        if (key == null) {
            throw new NullPointerException("Null Key supplied");
        }
        ExpirableEntry<K, V> removedEntry = backingStore.remove(key);
        grimReaper.cancelScheduledExpiration(removedEntry);
    }


    /**
     * @return true if there are no entries in the map. Returns false otherwise.
     */
    @Override
    public boolean isEmpty() {
        return backingStore.size() == 0;
    }

    /**
     * The number of entries in the map at the time of the query.
     *
     * @return total number of entries currently in the map, includes both expirable and non-expirable entries.
     */
    @Override
    public int size() {
        return backingStore.size();
    }

    /**
     * The number of entries currently scheduled for removal from the map.
     *
     * @return total number of expirable entries.
     */
    @Override
    public int getExpirablesSize() {
        return grimReaper.size();
    }

    /**
     * Shuts down the internal entity which does the cleanup of expired entries.
     *
     * @throws InterruptedException in case shutdown is interrupted.
     */
    @Override
    public void shutDown() throws InterruptedException {
        backingStore.clear();
        grimReaper.shutDown();
    }

    @Override
    public String toString() {
        return backingStore.toString();
    }

    /**
     * Created by kverma on 4/7/17.
     */
    public static class ExpireMapBuilder<K, V> {
        private ConcurrentMap<K, ExpirableEntry<K, V>> kvBackingStore;
        private CleanupScheduler<K, V> cleanupScheduler;
        private Integer initialCapacity;
        private Float loadFactor;
        private Integer concurrencyLevel;

        public ExpireMapBuilder<K, V> withKvBackingStore(ConcurrentMap<K, ExpirableEntry<K, V>> kvBackingStore) {
            if (kvBackingStore == null) {
                throw new IllegalArgumentException();
            }
            this.kvBackingStore = kvBackingStore;
            return this;
        }

        public ExpireMapBuilder<K, V> withCleanupScheduler(CleanupScheduler<K, V> cleanupScheduler) {
            if (cleanupScheduler == null) {
                throw new IllegalArgumentException();
            }
            this.cleanupScheduler = cleanupScheduler;
            return this;
        }

        public ExpireMapBuilder<K, V> withInitialCapcity(int initialCapacity) {
            if (initialCapacity < 0) {
                throw new IllegalArgumentException();
            }
            this.initialCapacity = initialCapacity;
            return this;
        }

        public ExpireMapBuilder<K, V> withLoadFactor(float loadFactor) {
            if (!(loadFactor > 0.0f)) {
                throw new IllegalArgumentException();
            }
            this.loadFactor = loadFactor;
            return this;
        }


        public ExpireMapBuilder<K, V> withConcurrencyLevel(int concurrencyLevel) {
            if (concurrencyLevel <= 0) {
                throw new IllegalArgumentException();
            }
            this.concurrencyLevel = concurrencyLevel;
            return this;
        }

        public ExpireMap<K, V> build() {
            if (kvBackingStore == null) {
                if (initialCapacity == null || loadFactor == null) {
                    kvBackingStore = new ConcurrentHashMap<>();
                } else if (concurrencyLevel == null) {
                    kvBackingStore = new ConcurrentHashMap<>(initialCapacity, loadFactor);
                } else {
                    kvBackingStore = new ConcurrentHashMap<>(initialCapacity, loadFactor, concurrencyLevel);
                }
            }

            if (cleanupScheduler == null) {
                cleanupScheduler = new Reaper<>();
            }

            return new ExpireMapImpl<>(this);
        }
    }
}
