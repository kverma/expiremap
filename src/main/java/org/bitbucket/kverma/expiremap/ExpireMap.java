/*
 * Copyright (c) 2017. Krish Verma
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.bitbucket.kverma.expiremap;

public interface ExpireMap<K, V> {

    /**
     * If there is no entry with the key in the map, add the key/value pair as a new entry.
     * If there is an existing entry with the key, the current entry will be replaced with the new key/value pair.
     * If the newly added entry is not removed after timeoutMs since it's added to the map, remove it.
     * If timeoutMs is 0, then entry never expires
     *
     * @param key Key for the map entry. Not Null. Throws NPE in case it is null
     * @param value Value for the map. Nullable
     * @param timeoutMs Expiration age of the entry, must be greater than or equal to 0. 0 means never expire.
     * @throws IllegalArgumentException in case of negative timeout.
     * @throws NullPointerException     in case of null key.
     */
    void put(K key, V value, long timeoutMs);


    /**
     * Get the value associated with the key if present; otherwise, return null.
     *
     * @param key retrieval key
     * @return null in case key is not present, or else has expired otherwise value associated with key.
     */
    V get(K key);


    /**
     * Remove the entry associated with key, if any.
     *
     * @param key - removes the associated entry if present in the map.
     */
    void remove(K key);


    // Util methods

    /**
     * The number of entries in the map at the time of the query.
     *
     * @return total number of entries currently in the map, includes both expirable and non-expirable entries.
     */
    int size();

    /**
     * @return true if there are no entries in the map. Returns false otherwise.
     */
    boolean isEmpty();

    /**
     * The number of entries currently scheduled for removal from the map.
     *
     * @return total number of expirable entries.
     */
    int getExpirablesSize();

    /**
     * For clean-up, cancels all pending expirations, and clears the map of all entries.
     *
     * @throws InterruptedException in case shut down process is interrupted.
     */
    void shutDown() throws InterruptedException;
}
