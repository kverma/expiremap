/*
 * Copyright (c) 2017. Krish Verma
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.bitbucket.kverma.expiremap;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.google.common.base.Function;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.bitbucket.kverma.expiremap.impl.ExpireMapImpl.ExpireMapBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ConcurrentLoadTest {

    public static final int PAR_OPS = 100;
    public static final int TIMEOUT = 30;
    public static final int MIN_EXPIRYAGE = TIMEOUT * 1000;
    private static final Logger LOG = LoggerFactory.getLogger(ConcurrentLoadTest.class);
    private static final Long FIXED_KEY = 1L;
    private ExpireMap<Long, Long> longMap;
    private Random randomizer = new Random();
    private ListeningExecutorService executorService;
    private ListeningExecutorService directExecutorSvc;

    @Before
    public void setUp() throws Exception {
        longMap = new ExpireMapBuilder<Long, Long>().build();
        final ThreadFactory namedThreadFactory = new ThreadFactoryBuilder()
                .setNameFormat("ConcurrentLoadTester-%d")
                .setDaemon(true)
                .build();
        ExecutorService ex = new ThreadPoolExecutor(PAR_OPS,
                                                    PAR_OPS,
                                                    0L,
                                                    TimeUnit.MILLISECONDS,
                                                    new ArrayBlockingQueue<>(1000000),
                                                    namedThreadFactory,
                                                    new ThreadPoolExecutor.CallerRunsPolicy());
        executorService = MoreExecutors.listeningDecorator(ex);
        directExecutorSvc = MoreExecutors.newDirectExecutorService();
    }

    @After
    public void tearDown() throws Exception {
        if (longMap != null) {
            longMap.shutDown();
        }
        if (executorService != null) {
            executorService.shutdownNow();
        }
        if (directExecutorSvc != null) {
            directExecutorSvc.shutdownNow();
        }
    }

    @Test
    public void putInMultipleThreadsAndGetFromOneThread() throws Exception {

        int numOfEntriesPerThread = MIN_EXPIRYAGE + randomizer.nextInt(1000);
        LOG.info("putInMultipleThreadsAndGetFromOneThread | Num Of Entries per Thread {}", numOfEntriesPerThread);
        // Put large # of entries in parallel
        List<ListenableFuture<Boolean>> workedThreadDone = new ArrayList<>();
        for (long i = 1; i <= PAR_OPS; i++) {
            long finalI = i;
            ListenableFuture<Boolean> fDone = executorService.submit(() -> {
                for (long j = 1; j <= numOfEntriesPerThread; j++) {
                    long value = finalI * j;
                    longMap.put(value, value, 0);
                    LOG.debug("putInMultipleThreadsAndGetFromOneThread | put [{}, {}, {}]", value, value, 0);
                }
                LOG.info("Done putting ...");
                return Boolean.TRUE;
            });
            workedThreadDone.add(fDone);
        }

        ListenableFuture<List<Boolean>> fWorkersDone = Futures.allAsList(workedThreadDone);
        ListenableFuture<Boolean> allReadsMatchedPuts =
                Futures.transform(fWorkersDone,
                                  (Function<List<Boolean>, Boolean>) booleans -> {
                                      for (long i = 1; i <= PAR_OPS; i++) {
                                          for (long j = 1; j <= numOfEntriesPerThread; j++) {
                                              Long expectedValue = i * j;
                                              if (!expectedValue.equals(longMap.get(expectedValue))) {
                                                  return Boolean.FALSE;
                                              }
                                          }
                                      }
                                      return Boolean.TRUE;
                                  }, executorService);
        assertTrue(allReadsMatchedPuts.get(TIMEOUT, TimeUnit.SECONDS)); // Unsafe - wait
    }


    @Test
    public void putInMultipleThreadsAndRemoveFromMultipleThread() throws Exception {

        int numOfEntriesPerThread = TIMEOUT + randomizer.nextInt(1000);
        LOG.info("putInMultipleThreadsAndRemoveFromMultipleThread  | Num Of Entries per Thread {}",
                 numOfEntriesPerThread);
        // Put large # of entries in parallel
        List<ListenableFuture<Boolean>> workedThreadDone = new ArrayList<>();
        for (long i = 0; i < PAR_OPS; i++) {
            long finalI = i;
            ListenableFuture<Boolean> fDone = executorService.submit(() -> {
                for (long j = 1; j <= numOfEntriesPerThread; j++) {
                    long value = finalI * j;
                    longMap.put(value, value, 0);
                    LOG.debug("putInMultipleThreadsAndRemoveFromMultipleThread | Updating [{}, {}, {}]", value, value,
                              0);
                }
                LOG.info("putInMultipleThreadsAndRemoveFromMultipleThread | Done putting ...");
                return Boolean.TRUE;
            });
            workedThreadDone.add(fDone);
        }

        ListenableFuture<List<Boolean>> fWorkersDone = Futures.allAsList(workedThreadDone);
        ListenableFuture<List<Boolean>> allRemovalsDone =
                Futures.transform(fWorkersDone,
                                  (Function<List<Boolean>, List<Boolean>>) booleans -> {
                                      List<ListenableFuture<Boolean>> futures = new ArrayList<>();
                                      for (long k = 0; k < PAR_OPS; k++) {
                                          long finalk = k;
                                          ListenableFuture<Boolean> fDone = executorService.submit(() -> {
                                              for (long j = 1; j <= numOfEntriesPerThread; j++) {
                                                  long value = finalk * j;
                                                  longMap.remove(value);
                                                  LOG.debug(
                                                          "putInMultipleThreadsAndRemoveFromMultipleThread | removing"
                                                          + " [{}, {}, {}]",
                                                          value, value, 0);
                                              }
                                              LOG.info(
                                                      "putInMultipleThreadsAndRemoveFromMultipleThread | Done "
                                                      + "removing ...");
                                              return Boolean.TRUE;
                                          });
                                          futures.add(fDone);
                                      }
                                      List<Boolean> updates = new ArrayList<>();
                                      ListenableFuture<List<Boolean>> allDone = Futures.allAsList(futures);
                                      try {
                                          updates = allDone.get(TIMEOUT, TimeUnit.SECONDS);
                                      } catch (Exception e) {
                                          LOG.error("Exception!!!", e);
                                          ;
                                      }
                                      return updates;
                                  }, executorService);
//        ListenableFuture<List<Boolean>> fRemovalsDone = Futures.allAsList(allRemovalsDone);
        allRemovalsDone.get(TIMEOUT, TimeUnit.SECONDS); // Unsafe - wait
        for (long i = 1; i < PAR_OPS; i++) {
            for (int j = 1; j <= numOfEntriesPerThread; j++) {
                assertNull(longMap.get(i * j));
            }
        }
    }

    @Test
    public void putInMultipleThreadsAndWaitToExpire() throws Exception {

        int numOfEntriesPerThread = TIMEOUT + randomizer.nextInt(1000);
        LOG.info("putInMultipleThreadsAndWaitToExpire | Num Of Entries per Thread {}", numOfEntriesPerThread);
        // Put large # of entries in parallel
        List<ListenableFuture<Boolean>> workedThreadDone = new ArrayList<>();
        for (long i = 0; i < PAR_OPS; i++) {
            long finalI = i;
            ListenableFuture<Boolean> fDone = executorService.submit(() -> {
                for (long j = 1; j <= numOfEntriesPerThread; j++) {
                    long value = finalI * j;
                    long timeOutMs = 1 + randomizer.nextInt(MIN_EXPIRYAGE);
                    longMap.put(value, value, timeOutMs);
                    LOG.debug("putInMultipleThreadsAndWaitToExpire | Updating [{}, {}, {}]", value, value, timeOutMs);
                }
                LOG.info("putInMultipleThreadsAndWaitToExpire | Done putting ...");
                return Boolean.TRUE;
            });
            workedThreadDone.add(fDone);
        }
        ListenableFuture<List<Boolean>> fWorkersDone = Futures.allAsList(workedThreadDone);
        fWorkersDone.get(TIMEOUT, TimeUnit.SECONDS); // wait for insertions to complete
        LOG.info("putInMultipleThreadsAndWaitToExpire | Map before sleeping has {} entries", longMap.size());
        LOG.debug("putInMultipleThreadsAndWaitToExpire | Map before sleeping has {} ", longMap); // Schrodinger's cat.
        Thread.sleep(MIN_EXPIRYAGE); // wait for expiration
        for (long i = 0; i <= PAR_OPS; i++) {
            for (int j = 1; j <= numOfEntriesPerThread; j++) {
                assertNull(longMap.get(i * j));
            }
        }
    }
}