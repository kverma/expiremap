/*
 * Copyright (c) 2017. Krish Verma
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.bitbucket.kverma.expiremap;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.google.common.base.Function;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.bitbucket.kverma.expiremap.impl.ExpireMapImpl.ExpireMapBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MultipleMapTest {

    public static final int PAR_OPS = 10;
    public static final int TIMEOUT = 60;
    public static final int MIN_EXPIRYAGE = TIMEOUT * 1000;
    private static final Logger LOG = LoggerFactory.getLogger(MultipleMapTest.class);
    @SuppressWarnings("unchecked")
    private ExpireMap<Long, Long>[] longMap = new ExpireMap[PAR_OPS];
    private Random randomizer = new Random();
    private ListeningExecutorService executorService;
    private ListeningExecutorService directExecutorSvc;

    @Before
    public void setUp() throws Exception {
        for (int i = 0; i < PAR_OPS; i++) {
            longMap[i] = new ExpireMapBuilder<Long, Long>().build();
        }

        final ThreadFactory namedThreadFactory = new ThreadFactoryBuilder()
                .setNameFormat("MultipleMapTest-%d")
                .setDaemon(true)
                .build();
        ExecutorService ex = new ThreadPoolExecutor(PAR_OPS,
                                                    PAR_OPS,
                                                    0L,
                                                    TimeUnit.MILLISECONDS,
                                                    new ArrayBlockingQueue<>(1000000),
                                                    namedThreadFactory,
                                                    new ThreadPoolExecutor.CallerRunsPolicy());
        executorService = MoreExecutors.listeningDecorator(ex);
        directExecutorSvc = MoreExecutors.newDirectExecutorService();
    }

    @After
    public void tearDown() throws Exception {
        if (longMap != null) {
            for (int i = 0; i < PAR_OPS; i++) {
                if (longMap[i] != null) {
                    longMap[i].shutDown();
                }
            }
        }
        if (executorService != null) {
            executorService.shutdownNow();
        }
        if (directExecutorSvc != null) {
            directExecutorSvc.shutdownNow();
        }
    }

    @Test
    public void putInMultipleMaps() throws Exception {

        int numOfEntriesPerMap = TIMEOUT + randomizer.nextInt(1000);
        LOG.info("Num Of Entries per Map {}", numOfEntriesPerMap);
        // Put large # of entries in parallel
        List<ListenableFuture<Boolean>> workedThreadDone = new ArrayList<>();
        for (int i = 1; i <= PAR_OPS; i++) {
            int finalI = i;
            ListenableFuture<Boolean> fDone = executorService.submit(() -> {
                for (int j = 1; j <= numOfEntriesPerMap; j++) {
                    long value = finalI * j;
                    longMap[finalI - 1].put(value, value, 0);
                    LOG.info("Updating [{}, {}, {}]", value, value, 0);
                }
                LOG.info("Done putting ...");
                return Boolean.TRUE;
            });
            workedThreadDone.add(fDone);
        }

        ListenableFuture<List<Boolean>> fWorkersDone = Futures.allAsList(workedThreadDone);
        ListenableFuture<Boolean> allReadsMatchedPuts =
                Futures.transform(fWorkersDone,
                                  (Function<List<Boolean>, Boolean>) booleans -> {
                                      for (int i = 1; i <= PAR_OPS; i++) {
                                          for (long j = 1; j <= numOfEntriesPerMap; j++) {
                                              Long expectedValue = i * j;
                                              if (!expectedValue.equals(longMap[i - 1].get(expectedValue))) {
                                                  return Boolean.FALSE;
                                              }
                                          }
                                      }
                                      return Boolean.TRUE;
                                  }, executorService);
        assertTrue(allReadsMatchedPuts.get(TIMEOUT, TimeUnit.SECONDS)); // Unsafe - wait
    }
}