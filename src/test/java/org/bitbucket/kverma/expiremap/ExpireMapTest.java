/*
 * Copyright (c) 2017. Krish Verma
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.bitbucket.kverma.expiremap;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.bitbucket.kverma.expiremap.impl.ExpireMapImpl.ExpireMapBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ExpireMapTest {
    public static final int MAX_NUM_ENTRIES = 10_000;
    private static final Logger LOG = LoggerFactory.getLogger(ExpireMapTest.class);
    private static final Long FIXED_KEY = 1L;
    private ExpireMap<Long, Long> longMap;
    private Random randomizer = new Random();

    @Before
    public void setUp() throws Exception {
        longMap = new ExpireMapBuilder<Long, Long>().build();
    }

    @After
    public void tearDown() throws Exception {
        if (longMap != null) {
            longMap.shutDown();
        }
    }

    @Test
    public void putOne() throws Exception {
        longMap.put(FIXED_KEY, FIXED_KEY, 100000);
        assertEquals("After Put 1 entry Size", 1, longMap.size());
        assertEquals("After Put 1 entry # of Expirable Entries", 1, longMap.getExpirablesSize());
        assertEquals("After Put 1 entry IsEmpty", false, longMap.isEmpty());

    }

    @Test
    public void putOneNonExpirable() throws Exception {
        longMap.put(FIXED_KEY, FIXED_KEY, 0);
        assertEquals("After Put 1 entry Size", 1, longMap.size());
        assertEquals("After Put 1 entry # of Expirable Entries", 0, longMap.getExpirablesSize());
        assertEquals("After Put 1 entry IsEmpty", false, longMap.isEmpty());
    }

    @Test
    public void putOneMaxExpirable() throws Exception {
        longMap.put(FIXED_KEY, FIXED_KEY, Long.MAX_VALUE);
        assertEquals("After Put 1 entry Size", 1, longMap.size());
        assertEquals("After Put 1 entry # of Expirable Entries", 0, longMap.getExpirablesSize());
        assertEquals("After Put 1 entry IsEmpty", false, longMap.isEmpty());
    }

    @Test
    public void putTen() throws Exception {
        for (long i = 1; i <= 10; i++) {
            longMap.put(i, i, 10000);
        }
        assertEquals("After Put 10 entry", 10, longMap.size());
        assertEquals("After Put 10 entry # of Expirable Entries", 10, longMap.getExpirablesSize());
        assertEquals("After Put 10 entry IsEmpty", false, longMap.isEmpty());
    }

    @Test
    public void testNonExpirableValues() throws Exception {
        int numOfEntries = 1_00_000;
        Long[] expecteds = new Long[numOfEntries];
        Long[] actuals = new Long[numOfEntries];
        Arrays.fill(expecteds, randomizer.nextLong());
        for (int i = 0; i < numOfEntries; i++) {
            longMap.put((long) i + 1, expecteds[i], 0);
        }
        for (int i = 0; i < numOfEntries; i++) {
            actuals[i] = longMap.get((long) i + 1);
        }
        assertArrayEquals(expecteds, actuals);
    }

    @Test
    public void testExpirableValues() throws Exception {
        int numOfEntries = 1_00_000;
        Long[] expecteds = new Long[numOfEntries];
        Long[] actuals = new Long[numOfEntries];
        Arrays.fill(expecteds, randomizer.nextLong());
        for (int i = 0; i < numOfEntries; i++) {
            longMap.put((long) i + 1, expecteds[i], 60_000);
        }
        for (int i = 0; i < numOfEntries; i++) {
            actuals[i] = longMap.get((long) i + 1);
        }
        assertArrayEquals(expecteds, actuals);
    }

    @Test
    public void remove() throws Exception {
        longMap.put(FIXED_KEY, FIXED_KEY, 10000);
        longMap.remove(FIXED_KEY);
        assertEquals("After Put/Remove 1 entry", 0, longMap.size());
        assertNull("Must be null", longMap.get(FIXED_KEY));
    }

    @Test(expected = NullPointerException.class)
    public void putNull() {
        longMap.put(null, FIXED_KEY, 100);
    }

    @Test(expected = NullPointerException.class)
    public void getNull() {
        longMap.get(null);
    }

    @Test(expected = NullPointerException.class)
    public void removeNull() {
        longMap.remove(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void putNegativeTimeOut() {
        longMap.put(FIXED_KEY, FIXED_KEY, -1);
    }


    @Test
    public void testOneEntryExpiration() throws InterruptedException {
        longMap.put(FIXED_KEY, FIXED_KEY, 2000);
        assertEquals("After Put 1 entry Size", 1, longMap.size());
        assertEquals("After Put 1 entry IsEmpty", false, longMap.isEmpty());
        Thread.sleep(2000);
        assertNull("Must be null", longMap.get(FIXED_KEY));
        // Size cannot be relied so pausing for another second
        Thread.sleep(1000);
        assertEquals("After Sleeping some more Size", 0, longMap.size());
        assertEquals("After Sleeping some more expiration Size", 0, longMap.getExpirablesSize());
        assertEquals("After Sleeping some more IsEmpty", true, longMap.isEmpty());
    }

    @Test
    public void testOneEntryBeforeExpiration() throws InterruptedException {
        longMap.put(FIXED_KEY, FIXED_KEY, 2000);
        assertEquals("After Put 1 entry Size", 1, longMap.size());
        assertEquals("After Put 1 entry IsEmpty", false, longMap.isEmpty());
        Thread.sleep(1000);
        assertEquals(FIXED_KEY, longMap.get(FIXED_KEY));
        assertEquals("Must be the same value ", new Long(1), longMap.get(FIXED_KEY));
        // Size cannot be relied so pausing for another second
        Thread.sleep(1000);
        assertNull("Must be null Now", longMap.get(FIXED_KEY));
    }

    @Test
    public void testMultipleEntryExpirationWithNoForeverEntry() throws InterruptedException {
        int numOfEntries = MAX_NUM_ENTRIES;

        int maxTimeOut = 5;
        long startTime = System.currentTimeMillis();
        for (long i = 1; i <= numOfEntries; i++) {
            longMap.put(i, i, TimeUnit.MILLISECONDS.convert(1
                                                            + randomizer.nextInt(maxTimeOut), TimeUnit.SECONDS));
        }
        long timeTakenToPut = System.currentTimeMillis() - startTime;
        Thread.sleep(TimeUnit.MILLISECONDS.convert(maxTimeOut, TimeUnit.SECONDS)
                     + TimeUnit.MILLISECONDS.convert(timeTakenToPut, TimeUnit.MILLISECONDS));
        for (long i = 1; i <= numOfEntries; i++) {
            assertNull("Must be null Now", longMap.get(i));
        }
    }

    @Test
    public void testMultipleEntryExpirationWithOneForeverEntry() throws InterruptedException {
        int numOfEntries = MAX_NUM_ENTRIES;
        int maxTimeOut = 5;
        long startTime = System.currentTimeMillis();
        for (long i = 1; i <= numOfEntries; i++) {
            long timeoutMs = TimeUnit.MILLISECONDS.convert(1
                                                           + randomizer.nextInt(maxTimeOut), TimeUnit.SECONDS);

            longMap.put(i, i, timeoutMs);
        }
        longMap.put(0L, 0L, 0L);
        long timeTakenToPut = System.currentTimeMillis() - startTime;
        Long val = longMap.get(0L);
        assertNotNull(val);
        assertEquals(new Long(0), val);
        Thread.sleep(TimeUnit.MILLISECONDS.convert(maxTimeOut, TimeUnit.SECONDS)
                     + TimeUnit.MILLISECONDS.convert(timeTakenToPut, TimeUnit.MILLISECONDS));
        val = longMap.get(0L);
        assertNotNull(val);
        assertEquals(new Long(0), val);
        for (long i = 1; i <= numOfEntries; i++) {
            assertNull("Must be null Now", longMap.get(i));
        }

        val = longMap.get(0L);
        assertNotNull(val);
        assertEquals(new Long(0), val);
    }

    @Test
    public void testMultiplePutsWithoutExpiry() {
        int numOfEntries = MAX_NUM_ENTRIES;
        Long[] expecteds = new Long[numOfEntries];
//        Long[] actuals = new Long[numOfEntries];
        Arrays.fill(expecteds, randomizer.nextLong());
        for (int i = 0; i < numOfEntries; i++) {
            longMap.put(FIXED_KEY, expecteds[i], 0);
        }
        assertEquals(expecteds[numOfEntries - 1], longMap.get(FIXED_KEY));
    }

    @Test
    public void testMultiplePutsWhileExpiry() {
        int numOfEntries = 1_00_000;
        Long[] expecteds = new Long[numOfEntries];
        Arrays.fill(expecteds, randomizer.nextLong());
        for (int i = 0; i < numOfEntries; i++) {
            longMap.put(FIXED_KEY, expecteds[i], randomizer.nextInt(5));
        }
        // Fixed Entry
        longMap.put(FIXED_KEY, expecteds[0], 0);
        assertEquals(expecteds[0], longMap.get(FIXED_KEY));
    }

    @Test
    public void testMultiplePutsWithIncreasingExpiry() throws InterruptedException {
        int numOfEntries = MAX_NUM_ENTRIES;
        longMap = new ExpireMapBuilder<Long, Long>().withInitialCapcity(numOfEntries / 10)
                                                    .withLoadFactor(0.65f)
                                                    .withConcurrencyLevel(10)
                                                    .build();
        Long[] expecteds = new Long[numOfEntries];
        Arrays.fill(expecteds, Math.min(5000L, Math.abs(randomizer.nextLong())));
        Arrays.sort(expecteds);
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < numOfEntries; i++) {
            longMap.put(FIXED_KEY, expecteds[i], expecteds[i]);
        }
        long timeTakenToPut = System.currentTimeMillis() - startTime;
        assertEquals(expecteds[numOfEntries - 1], longMap.get(FIXED_KEY));
        Thread.sleep(expecteds[numOfEntries - 1]
                     + TimeUnit.MILLISECONDS.convert(timeTakenToPut, TimeUnit.MILLISECONDS));
        assertNull(longMap.get(FIXED_KEY));
    }


    @Test
    public void testMultiplePutsWithRandomExpiry() throws InterruptedException {
        int numOfEntries = randomizer.nextInt(1 + 10_000);
        longMap = new ExpireMapBuilder<Long, Long>().withInitialCapcity(numOfEntries / 10)
                                                    .withLoadFactor(0.75f)
                                                    .build();
        Long[] expecteds = new Long[numOfEntries];
        Arrays.fill(expecteds, Math.min(5000L, Math.abs(randomizer.nextLong())));
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < numOfEntries; i++) {
            longMap.put(FIXED_KEY, expecteds[i], expecteds[i]);
        }
        long timeTakenToPut = System.currentTimeMillis() - startTime;
        Thread.sleep(expecteds[numOfEntries - 1]
                     + TimeUnit.MILLISECONDS.convert(timeTakenToPut, TimeUnit.MILLISECONDS));
        assertNull(longMap.get(FIXED_KEY));
    }

    @Test
    public void testMultiplePutsWithDecreasingExpiry() throws InterruptedException {
        int numOfEntries = MAX_NUM_ENTRIES;
        longMap = new ExpireMapBuilder<Long, Long>().withInitialCapcity(numOfEntries).build();
        Long[] expecteds = new Long[numOfEntries];
        Arrays.fill(expecteds, (long) (1 + randomizer.nextInt(5000)));
        Arrays.sort(expecteds);
        long startTime = System.currentTimeMillis();
        for (int i = numOfEntries - 1; i >= 0; i--) {
            longMap.put(FIXED_KEY, expecteds[i], expecteds[i]);
        }
        long timeTakenToPut = System.currentTimeMillis() - startTime;
        Thread.sleep(expecteds[0]
                     + TimeUnit.MILLISECONDS.convert(timeTakenToPut, TimeUnit.MILLISECONDS));
        assertNull(longMap.get(FIXED_KEY));
    }

    @Test(expected = IllegalThreadStateException.class)
    public void testPutPostShutDown() {
        longMap = new ExpireMapBuilder<Long, Long>().build();
        try {
            longMap.shutDown();
        } catch (InterruptedException e) {
            LOG.error("Exception!!!", e);
        }
        longMap.put(FIXED_KEY, FIXED_KEY, 0);
    }

    @Test
    public void testGetPostShutDown() {
        longMap = new ExpireMapBuilder<Long, Long>().build();
        longMap.put(FIXED_KEY, FIXED_KEY, 0);
        try {
            longMap.shutDown();
        } catch (InterruptedException e) {
            LOG.error("Exception!!!", e);
        }
        assertNull(longMap.get(FIXED_KEY));
        assertNull(longMap.get(FIXED_KEY + 1));
    }

    @Test(expected = IllegalThreadStateException.class)
    public void testRemovePostShutDown() {
        longMap = new ExpireMapBuilder<Long, Long>().build();
        longMap.put(FIXED_KEY, FIXED_KEY, 0);
        try {
            longMap.shutDown();
        } catch (InterruptedException e) {
            LOG.error("Exception!!!", e);
        }
        longMap.remove(FIXED_KEY);
    }
}