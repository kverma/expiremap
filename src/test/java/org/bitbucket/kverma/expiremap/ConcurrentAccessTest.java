/*
 * Copyright (c) 2017. Krish Verma
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.bitbucket.kverma.expiremap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.google.common.base.Function;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.bitbucket.kverma.expiremap.impl.ExpireMapImpl.ExpireMapBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ConcurrentAccessTest {
    public static final int PAR_OPS = 10;
    public static final int TIMEOUT = 30;
    public static final int MIN_EXPIRYAGE = TIMEOUT * 1000;
    private static final Logger LOG = LoggerFactory.getLogger(ConcurrentAccessTest.class);
    private static Random randomizer = new Random();
    private static final Long FIXED_KEY = randomizer.nextLong();
    private ExpireMap<Long, Long> longMap;

    private ListeningExecutorService executorService;
    private ListeningExecutorService directExecutorSvc;

    @Before
    public void setUp() throws Exception {
        longMap = new ExpireMapBuilder<Long, Long>().build();
        final ThreadFactory namedThreadFactory = new ThreadFactoryBuilder()
                .setNameFormat("ConcurrentAccessTester-%d")
                .setDaemon(true)
                .build();
        ExecutorService ex = new ThreadPoolExecutor(10,
                                                    PAR_OPS,
                                                    0L,
                                                    TimeUnit.MILLISECONDS,
                                                    new ArrayBlockingQueue<>(1000000),
                                                    namedThreadFactory,
                                                    new ThreadPoolExecutor.CallerRunsPolicy());
        executorService = MoreExecutors.listeningDecorator(ex);
        directExecutorSvc = MoreExecutors.newDirectExecutorService();
    }

    @After
    public void tearDown() throws Exception {
        if (longMap != null) {
            longMap.shutDown();
        }
        if (executorService != null) {
            executorService.shutdownNow();
        }
        if (directExecutorSvc != null) {
            directExecutorSvc.shutdownNow();
        }
    }

    @Test
    public void putInOneThreadAndGetFromAnotherThread() throws Exception {

        ListenableFuture<Long> submit = directExecutorSvc.submit(() -> {
            Long val = randomizer.nextLong();
            longMap.put(FIXED_KEY, val, 100000);
            return val;
        });
        Long expectedValue = submit.get(1000, TimeUnit.SECONDS);

        ListenableFuture<Long> future = executorService.submit(() -> longMap.get(FIXED_KEY));

        // Wait for put to finish
        Long gotValue = future.get(1000, TimeUnit.SECONDS);
        assertEquals(expectedValue, gotValue);
    }

    @Test
    public void putInOneThreadAndGetNonExistentFromAnotherThread() throws Exception {

        ListenableFuture<Long> submit = directExecutorSvc.submit(() -> {
            Long val = randomizer.nextLong();
            longMap.put(FIXED_KEY, val, 100000);
            return val;
        });
        ListenableFuture<Long> future = Futures.transform(submit,
                                                          (Function<Long, Long>) aLong ->
                                                                  longMap.get(FIXED_KEY + 1), executorService);

        // Wait for get to finish
        Long gotValue = future.get(1000, TimeUnit.SECONDS);
        assertNull(gotValue);
    }

    @Test
    public void putInOneThreadAndRemoveFromAnotherThread() throws Exception {

        ListenableFuture<Long> submit = directExecutorSvc.submit(() -> {
            Long val = randomizer.nextLong();
            longMap.put(FIXED_KEY, val, 0);
            return val;
        });
        submit.get(1000, TimeUnit.SECONDS);

        ListenableFuture<Boolean> future = executorService.submit(() -> {
            longMap.remove(FIXED_KEY);
            return Boolean.TRUE;
        });

        future.get(1000, TimeUnit.SECONDS);
        assertNull(longMap.get(FIXED_KEY));
    }


    @Test
    public void putInOneThreadAndGetFromMultipleThreads() throws Exception {

        ListenableFuture<Long> submit = directExecutorSvc.submit(() -> {
            Long val = randomizer.nextLong();
            LOG.info("Putting {}", val);
            longMap.put(FIXED_KEY, val, 100000);
            return val;
        });

        ListenableFuture<List<Long>> asyncGets =
                Futures.transform(submit,
                                  (Function<Long, List<Long>>) aLong -> {
                                      List<ListenableFuture<Long>> futuresGets = new ArrayList<>();
                                      for (int i = 0; i < PAR_OPS; i++) {
                                          futuresGets.add(executorService.submit(() -> {
                                              Long val = longMap.get(FIXED_KEY);
                                              LOG.info("putInOneThreadAndGetFromMultipleThreads | Got {}", val);
                                              return val;
                                          }));
                                      }
                                      // Convert from List of Futures to Future of List<Long> and go a blocking get
                                      List<Long> readValues = new ArrayList<>();
                                      try {
                                          ListenableFuture<List<Long>> listListenableFuture
                                                  = Futures.allAsList(futuresGets);
                                          readValues = listListenableFuture.get(TIMEOUT, TimeUnit.SECONDS);
                                      } catch (Exception e) {
                                          LOG.error("Exception!!!", e);
                                          ;
                                      }
                                      return readValues;
                                  }, executorService);

        Long expectedValue = submit.get(1000, TimeUnit.SECONDS);
        List<Long> parallelReads = asyncGets.get(TIMEOUT, TimeUnit.SECONDS);
        assertEquals(PAR_OPS, parallelReads.size()); // Futures should have read;
        parallelReads.forEach(readValue -> assertEquals(expectedValue, readValue));
    }


    @Test
    public void putInOneThreadAndUpdateFromMultipleThreads() throws Exception {

        ListenableFuture<Long> submit = directExecutorSvc.submit(() -> {
            Long val = randomizer.nextLong();
            longMap.put(FIXED_KEY, val, 100000);
            return val;
        });

        Long expectedValue = submit.get(1000, TimeUnit.SECONDS);
        ListenableFuture<List<Long>> asyncUpdateValues =
                Futures.transform(submit,
                                  (Function<Long, List<Long>>) aLong -> {
                                      List<ListenableFuture<Long>> fUpdates = new ArrayList<>();
                                      for (int i = 0; i < PAR_OPS; i++) {
                                          fUpdates.add(executorService.submit(() -> {
                                              Long val = randomizer.nextLong();
                                              int timeoutMs = MIN_EXPIRYAGE + randomizer.nextInt(10000);
                                              LOG.trace(
                                                      "putInOneThreadAndUpdateFromMultipleThreads | Updating [{}, {},"
                                                      + " {}]",
                                                      FIXED_KEY, val, timeoutMs);
                                              longMap.put(FIXED_KEY, val, timeoutMs);

                                              return val;
                                          }));
                                      }
                                      // Convert from List of Futures to Future of List<Long> and go a blocking get
                                      List<Long> updates = new ArrayList<>();
                                      try {
                                          ListenableFuture<List<Long>> fListUpdates
                                                  = Futures.allAsList(fUpdates);
                                          updates = fListUpdates.get(TIMEOUT, TimeUnit.SECONDS);
                                      } catch (Exception e) {
                                          LOG.error("Exception!!!", e);
                                          ;
                                      }
                                      return updates;
                                  }, executorService);
        List<Long> oneOfExpectedValues = asyncUpdateValues.get(TIMEOUT, TimeUnit.SECONDS);
        assertEquals(PAR_OPS, oneOfExpectedValues.size()); // Futures should have finished updating;
        Long latestValue = longMap.get(FIXED_KEY);
        assertTrue(oneOfExpectedValues.contains(latestValue) || expectedValue.equals(latestValue));
    }

    @Test
    public void putInOneThreadAndUpdateFromMultipleThreadsAndEnsureExpiry() throws Exception {

        ListenableFuture<Long> submit = directExecutorSvc.submit(() -> {
            Long val = randomizer.nextLong();
            longMap.put(FIXED_KEY, val, 100000);
            return val;
        });

        ListenableFuture<List<Long>> asyncUpdateValues =
                Futures.transform(submit,
                                  (Function<Long, List<Long>>) aLong -> {
                                      List<ListenableFuture<Long>> fUpdates = new ArrayList<>();
                                      for (int i = 0; i < PAR_OPS; i++) {
                                          fUpdates.add(executorService.submit(() -> {
                                              Long val = randomizer.nextLong();
                                              int timeoutMs = 1 + randomizer.nextInt(MIN_EXPIRYAGE);
                                              LOG.trace(
                                                      "putInOneThreadAndUpdateFromMultipleThreadsAndEnsureExpiry | "
                                                      + "Updating [{}, {}, {}]",
                                                      FIXED_KEY, val, timeoutMs);
                                              longMap.put(FIXED_KEY, val, timeoutMs);

                                              return val;
                                          }));
                                      }
                                      // Convert from List of Futures to Future of List<Long> and go a blocking get
                                      List<Long> updates = new ArrayList<>();
                                      try {
                                          ListenableFuture<List<Long>> fListUpdates
                                                  = Futures.allAsList(fUpdates);
                                          updates = fListUpdates.get(TIMEOUT, TimeUnit.SECONDS);
                                      } catch (Exception e) {
                                          LOG.error("Exception!!!", e);
                                          ;
                                      }
                                      return updates;
                                  }, executorService);
        List<Long> oneOfExpectedValues = asyncUpdateValues.get(TIMEOUT, TimeUnit.SECONDS);
        assertEquals(PAR_OPS, oneOfExpectedValues.size()); // Futures should have finished updating;
        Thread.sleep(MIN_EXPIRYAGE);
        assertNull(longMap.get(FIXED_KEY));
    }
}