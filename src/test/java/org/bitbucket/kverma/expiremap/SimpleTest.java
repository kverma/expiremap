/*
 * Copyright (c) 2017. Krish Verma
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.bitbucket.kverma.expiremap;

import static org.junit.Assert.assertNull;

import java.util.Random;

import org.bitbucket.kverma.expiremap.impl.ExpireMapImpl.ExpireMapBuilder;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Unit test for simple ExpireMap Test.
 */
public class SimpleTest {
    private static final Logger LOG = LoggerFactory.getLogger(SimpleTest.class);

    /**
     * Rigourous Test :-) // Ignore - please see other Tests.
     */
    @Test
    public void testApp() {
        ExpireMap<Long, Long> map = new ExpireMapBuilder<Long, Long>().build();
        Random random = new Random();
        long startTime = System.currentTimeMillis();
        int numOfEntries = 100 + random.nextInt(100);
        for (long i = 0; i < numOfEntries; i++) {
            map.put(i, i, 1 + random.nextInt(2000));
        }
        long timeTaken = System.currentTimeMillis() - startTime;
        LOG.info("Took {} ms to insert {} entries ", timeTaken, numOfEntries);
        LOG.info("Map Looks Like {}", map.toString());
        try {
            Thread.sleep(1000 * 3);
        } catch (InterruptedException e) {
            LOG.error("Exception!!!", e);
            ;
        }
        LOG.info("After 10 Seconds Map Looks Like", map.toString());
        for (long i = 0; i < numOfEntries; i++) {
            assertNull(map.get(i));
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void useIlleagalConcurrencyLevel() {
        new ExpireMapBuilder<String, String>().withConcurrencyLevel(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void useIlleagalInitCapacity() {
        new ExpireMapBuilder<String, String>().withInitialCapcity(-1);
    }


    @Test(expected = IllegalArgumentException.class)
    public void useIlleagalInitLoadFactor() {
        new ExpireMapBuilder<String, String>().withLoadFactor(-1.0f);
    }

    @Test(expected = IllegalArgumentException.class)
    public void useIlleagalBackingStore() {
        new ExpireMapBuilder<String, String>().withKvBackingStore(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void useIlleagalScheduler() {
        new ExpireMapBuilder<String, String>().withCleanupScheduler(null);
    }

    @Test(expected = IllegalThreadStateException.class)
    public void putAfterShutdown() {
        ExpireMap<Long, Long> map = new ExpireMapBuilder<Long, Long>().build();
        map.put(1L, 1L, 0);
        try {
            map.shutDown();
        } catch (InterruptedException e) {
            LOG.error("Exception", e);
        }
        map.put(1L, 1L, 1L);
    }

    @Test(expected = IllegalThreadStateException.class)
    public void removeAfterShutdown() {
        ExpireMap<Long, Long> map = new ExpireMapBuilder<Long, Long>().build();
        map.put(1L, 1L, 0);
        try {
            map.shutDown();
        } catch (InterruptedException e) {
            LOG.error("Exception", e);
        }
        map.remove(1L);
    }
}